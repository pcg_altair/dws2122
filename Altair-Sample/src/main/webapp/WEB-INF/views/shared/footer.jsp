<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<p>Copyright &copy 2020 Pablo Carrillo</p>

<script>
$.datepicker.regional['en'] = {
weekHeader: 'Sm',
dateFormat: 'dd/mm/yy',
firstDay: 1,
isRTL: false,
showMonthAfterYear: false,
yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['en']);
$( function() {
$.datepicker.setDefaults({
onClose:function(date, inst){
$("#selectedDtaeVal").html(date);
}
});

$( "#datepicker" ).datepicker();
});
</script>