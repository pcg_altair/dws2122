<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div>
	<img src="<c:url value="/images/logo.png" />"/>	
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  	<a class="navbar-brand" href="<c:url value="/" />">Altair-Sample</a>
	<div>
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link" href="<c:url value="/" />">Inicio</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="<c:url value="/students/listAll" />">Action1</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="<c:url value="/teachers/listAll" />">Action2</a>
			</li>
			<li class="nav-item active">
				<a class="nav-link" href="<c:url value="/action3/list" />">Action3</a>
			</li>
		</ul>
	</div>
</nav>