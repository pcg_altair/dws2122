package com.altair.sample.controllers;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = { "/home", "/" })
public class HomeController {

	@GetMapping("/")
	public ModelAndView index() {
		ModelAndView result = new ModelAndView("home/index");

		result.addObject("title", "Home");
		result.addObject("serverTime", LocalDateTime.now());

		return result;
	}
}
