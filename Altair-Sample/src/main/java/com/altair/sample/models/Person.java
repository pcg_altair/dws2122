package com.altair.sample.models;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Access(AccessType.FIELD)
@Data
@EqualsAndHashCode(callSuper=true)
public abstract class Person extends DomainEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8779913521797987166L;
	
	@NotEmpty(message = "This field cannot be empty or null")
	@Size(min=3, max=20, message = "This field must be min 3 characters and max 20 character")
	protected String name;
	
	
	protected String surname;
	
	@Email
	protected String email;
	
	@Pattern(regexp = "[0-9]{9}$")
	protected String phone;

	
	@Transient // indica que este atributo solo existe en java y no debe crearlo en BBDD
	protected String fullName;
	
	public String getFullName() {
		return this.name + " " + this.surname;
	}

}
