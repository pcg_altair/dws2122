package com.altair.sample.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.altair.sample.enums.PropertyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Access(AccessType.FIELD)
@Data
@EqualsAndHashCode(callSuper=true)
public class Property extends DomainEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5729190378052304133L;
	
	@NotEmpty
	private String address;
	private String country;
	private String postalCode;
	private PropertyType propertyType;
	
	@Valid // Validar las anotaciones de la clase Landlord, solo para clases propias
	private Landlord landlord;
	
	@Valid
	@OneToMany(mappedBy = "property")
	private List<Booking> bookings;
}
