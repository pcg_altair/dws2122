package com.altair.sample.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Access(AccessType.FIELD)
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "BOOKING")
public class Booking extends DomainEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8557659054578378992L;
	
	@Past(message = "The booking date must be past")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime bookingDate;
	private LocalDate startDate;
	private LocalDate endDate;
	
	@Valid
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	
	private Property property;
}
