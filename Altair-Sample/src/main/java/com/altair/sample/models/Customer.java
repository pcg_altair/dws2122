package com.altair.sample.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Access(AccessType.FIELD)
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "CUSTOMER")
public class Customer extends Person{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4148782218804177365L;
	
	private String nameCreditCard;
	
	@CreditCardNumber
	@Column(name="NUMBER") // Si no lo indicamos, se pondrá el nombre del atributo
	private String numberCreditCard;
	
	@NotNull
	@Range(min = 0, max = 999, message = "CVV not valid")
	private Integer cvv;
	
	@Range(min = 1, max = 12, message = "Month not valid")
	private Integer expirationMonth;
	
	@Min(2021)
	private Integer expirationYear;
	private List<Booking> bookings;
}
