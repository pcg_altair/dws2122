package com.altair.sample.models;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Access(AccessType.FIELD)
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "LANDLORD")
public class Landlord extends Person {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5377340325923356911L;
	
	@NotEmpty
	private String passport;
	
	
	private List<Property> properties;

}
