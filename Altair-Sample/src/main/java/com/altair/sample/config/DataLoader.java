package com.altair.sample.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationListener<ApplicationEvent> {

	@Value("${initialize.bbdd}")
	private Boolean initializeBBDD;

	@Override
	public void onApplicationEvent(ApplicationEvent event) {

		if (initializeBBDD) {
			// Datos de inicio
		}

	}

}
